package tratarocorrencias.util;

import com.mysql.jdbc.Connection;
import java.sql.SQLException;


/**
 *
 * @author Guilherme Brandão
 */
public interface IGerenciadorConexao {
        
    /**
     * Retorna uma conexao com o BD
     * @return
     * @throws java.sql.SQLException
     
     */
    public Connection conectar() throws SQLException;
    
    /**
     * Encerra a conexao com o BD
     * @param c
     * @throws java.sql.SQLException
     
     */
    public void desconectar(Connection c) throws SQLException;
}
