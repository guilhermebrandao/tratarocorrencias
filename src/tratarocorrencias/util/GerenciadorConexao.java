package tratarocorrencias.util;


import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;


/**
 *
 * @author Guilherme Brandão
 */
public class GerenciadorConexao implements IGerenciadorConexao {

    private static GerenciadorConexao instancia;
    private final String LOCAL;
    private final String USUARIO;
    private final String SENHA;
    private final String DRIVER;

    public GerenciadorConexao() {
        ResourceBundle rb = ResourceBundle.getBundle("tratarocorrencias.util.banco");
        LOCAL = rb.getString("local");
        USUARIO = rb.getString("usuario");
        SENHA = rb.getString("senha");
        DRIVER = rb.getString("driver");
    }

    public static GerenciadorConexao getInstancia() {
        if (instancia == null) {
            instancia = new GerenciadorConexao();
        }
        return instancia;
    }

    @Override
    public Connection conectar() throws SQLException {
        Connection c = null;
        try {
            Class.forName(DRIVER);
            c = (Connection) DriverManager.getConnection(LOCAL, USUARIO, SENHA);
        } catch (ClassNotFoundException | SQLException e) {
            throw new SQLException(e);
        }
        return c;
    }

    @Override
    public void desconectar(Connection c) throws SQLException {
        try {
            c.close();
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

}
