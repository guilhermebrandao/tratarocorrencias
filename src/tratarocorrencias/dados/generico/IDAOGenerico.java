/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tratarocorrencias.dados.generico;

import java.util.List;

public interface IDAOGenerico<T> {

	public void inserir(T obj);
	
	public void alterar(T obj);
	
	public void remover(T obj);
	
	public T consultarPorId(Integer id);
	
	public List<T> consultarTodos();	

}
