/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tratarocorrencias.basicas;


/**
 *
 * @author derick.irineu
 */
public class Ocorrencia {
    
    private String dtMovto;
    private String modelo;
    private String inscSequ;
    private String parcProc;
    private String orgao;
    private String agencia;
    private String vlrCalculado;
    private String vlrAutenticado;
    private String vlrDiferenca;
    private String ocorrencia;

    /**
     * @return the dtMovto
     */
    public String getDtMovto() {
        return dtMovto;
    }

    /**
     * @param dtMovto the dtMovto to set
     */
    public void setDtMovto(String dtMovto) {
        this.dtMovto = dtMovto;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return the inscSequ
     */
    public String getInscSequ() {
        return inscSequ;
    }

    /**
     * @param inscSequ the inscSequ to set
     */
    public void setInscSequ(String inscSequ) {
        this.inscSequ = inscSequ;
    }

    /**
     * @return the parcProc
     */
    public String getParcProc() {
        return parcProc;
    }

    /**
     * @param parcProc the parcProc to set
     */
    public void setParcProc(String parcProc) {
        this.parcProc = parcProc;
    }

    /**
     * @return the orgao
     */
    public String getOrgao() {
        return orgao;
    }

    /**
     * @param orgaoAgencia the orgao to set
     */
    public void setOrgao(String orgaoAgencia) {
        this.orgao = orgaoAgencia;
    }

    /**
     * @return the vlrCalculado
     */
    public String getVlrCalculado() {
        return vlrCalculado;
    }

    /**
     * @param vlrCalculado the vlrCalculado to set
     */
    public void setVlrCalculado(String vlrCalculado) {
        this.vlrCalculado = vlrCalculado;
    }

    /**
     * @return the vlrAutenticado
     */
    public String getVlrAutenticado() {
        return vlrAutenticado;
    }

    /**
     * @param vlrAutenticado the vlrAutenticado to set
     */
    public void setVlrAutenticado(String vlrAutenticado) {
        this.vlrAutenticado = vlrAutenticado;
    }

    /**
     * @return the vlrDiferenca
     */
    public String getVlrDiferenca() {
        return vlrDiferenca;
    }

    /**
     * @param vlrDiferenca the vlrDiferenca to set
     */
    public void setVlrDiferenca(String vlrDiferenca) {
        this.vlrDiferenca = vlrDiferenca;
    }

    /**
     * @return the ocorrencia
     */
    public String getOcorrencia() {
        return ocorrencia;
    }

    /**
     * @param ocorrencia the ocorrencia to set
     */
    public void setOcorrencia(String ocorrencia) {
        this.ocorrencia = ocorrencia;
    }

    @Override
    public String toString(){
        return this.getDtMovto() + "; " + this.getModelo() + "; " + this.getParcProc() + "; " + this.getInscSequ()+ "; " + this.getOrgao() + "; " + this.getAgencia() + "; " + this.getVlrCalculado() + "; " + this.getVlrAutenticado() + "; " + this.getVlrDiferenca() + "; " + this.getOcorrencia();
    }

    /**
     * @return the agencia
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * @param agencia the agencia to set
     */
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }
}